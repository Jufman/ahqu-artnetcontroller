﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AHQUArtNetController.UserControls
{
    /// <summary>
    /// Interaction logic for ControlLink.xaml
    /// </summary>
    public partial class ControlLink : UserControl
    {
        public delegate void RemoveClicked(ControlLink controlLink);
        public event RemoveClicked OnRemoveClicked;

        public Objects.MidiLink _midiLink;

        public ControlLink(Objects.MidiLink midiLink)
        {
            _midiLink = midiLink;

            _midiLink.OnValuesUpdate += _midiLink_OnValuesUpdate;

            InitializeComponent();

            BuildDropdown();
            Display();
        }

        private void _midiLink_OnValuesUpdate(int DMXValue, int MIDIValue)
        {
            Dispatcher.Invoke(() =>
            {
                DMXValueTextBlock.Text = $"DMX Value: {DMXValue}";
                MIDIValueTextBlock.Text = $"Midi Value: {MIDIValue}";
            });
        }

        private void Display()
        {
            DMXChannelTextBox.Text = _midiLink.DMXChannel.ToString();
            DeskChannelNumberTextBox.Text = _midiLink.MIDIChannel.ToString();

            foreach (var item in FaderTypeComboBox.Items)
            {
                if (item is ComboBoxItem == false)
                {
                    continue;
                }

                var cbi = item as ComboBoxItem;

                if ((int) cbi.Tag == _midiLink.MIDIChannelOffset)
                {
                    FaderTypeComboBox.SelectedItem = item;
                    break;
                }
            }
        }

        private void BuildDropdown()
        {
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "FX Sends",
                Tag = 0
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "FX Returns",
                Tag = 7
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "DCA Groups",
                Tag = 15
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Inputs",
                Tag = 31
            }); 
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Stereo Channels",
                Tag = 63
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Mute Groups",
                Tag = 79
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Groups",
                Tag = 103
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Mixs",
                Tag = 95
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Mains",
                Tag = 102
            });
            FaderTypeComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Matrixs",
                Tag = 107
            });
        }

        private void FaderTypeComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FaderTypeComboBox.SelectedItem is ComboBoxItem == false)
            {
                return;
            }

            var item = FaderTypeComboBox.SelectedItem as ComboBoxItem;

            _midiLink.MIDIChannelOffset = (int) item.Tag;
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            OnRemoveClicked?.Invoke(this);
        }

        private void DeskChannelNumberTextBox_OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (int.TryParse(DeskChannelNumberTextBox.Text, out var channelNumber))
            {
                _midiLink.MIDIChannel = channelNumber;
            }
        }

        private void DMXChannelTextBox_OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (int.TryParse(DMXChannelTextBox.Text, out var channelNumber))
            {
                _midiLink.DMXChannel = channelNumber;
            }
        }
    }
}
