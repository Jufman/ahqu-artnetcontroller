﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHQUArtNetController.Objects
{
    public class Settings
    {
        public int UniverseNumber { get; set; } = 1;
        public int MIDIDevice { get; set; }
        public List<Objects.MidiLink> MidiLinks { get; set; }= new List<MidiLink>();
    }
}
