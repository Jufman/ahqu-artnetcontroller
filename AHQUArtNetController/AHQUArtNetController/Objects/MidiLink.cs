﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHQUArtNetController.Objects
{
    public class MidiLink
    {

        public int DMXChannel { get; set; } = 1;

        public int MIDIChannelOffset { get; set; }

        public int MIDIChannel { get; set; } = 1;

        public delegate void ValuesUpdate(int DMXValue, int MIDIValue);
        public event ValuesUpdate OnValuesUpdate;

        public void UpdateValues(int DMXValue, int MIDIValue)
        {
            OnValuesUpdate?.Invoke(DMXValue, MIDIValue);
        }
    }
}
