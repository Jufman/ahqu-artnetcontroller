﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AHQUArtNetController.Objects;
using Haukcode.ArtNet;
using Haukcode.ArtNet.Packets;
using Haukcode.ArtNet.Sockets;
using Haukcode.Sockets;
using NAudio.Midi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AHQUArtNetController
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MidiOut DeskMidiDevice;

        private Objects.Settings _settings;

        private string FilePath = "Settings.Json";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            BuildMidiDropdown();
            LoadSettings();
            PopulateDisplay();

            UniverseTextBox.TextChanged += (o, args) =>
            {
                if (int.TryParse(UniverseTextBox.Text, out var universe))
                {
                    _settings.UniverseNumber = universe;
                }
            };

            MidiDevicesComboBox.SelectionChanged += (o, args) =>
            {
                if (MidiDevicesComboBox.SelectedItem is ComboBoxItem == false)
                {
                    return;
                }

                var item = MidiDevicesComboBox.SelectedItem as ComboBoxItem;

                _settings.MIDIDevice = (int)item.Tag;
            };
        }

        private void PopulateDisplay()
        {
            UniverseTextBox.Text = _settings.UniverseNumber.ToString();
            MidiDevicesComboBox.SelectedIndex = _settings.MIDIDevice;

            _settings.MidiLinks.ForEach(AddControlLinkItem);
        }

        private void LoadSettings()
        {
            if (!File.Exists(FilePath))
            {
                var newSettingsObject = new Objects.Settings();
                var jsonData = JObject.FromObject(newSettingsObject);

                File.WriteAllText(FilePath, jsonData.ToString());

                _settings = newSettingsObject;
                return;
            }

            _settings = JsonConvert.DeserializeObject<Objects.Settings>(File.ReadAllText(FilePath));
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(UniverseTextBox.Text, out var Universe))
            {
                _settings.UniverseNumber = Universe;
                SetupArtNetSocket();
                SetupMidi();

                StartButton.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Error Can't Parse Universe Number!!");
            }
        }

        private void SetupMidi()
        {
            var deviceIndex = MidiDevicesComboBox.SelectedIndex;
            DeskMidiDevice = new MidiOut(deviceIndex);
        }

        private void SetupArtNetSocket()
        {
            var artNetSocket = new ArtNetSocket
            {
                EnableBroadcast = true
            };

            artNetSocket.Open(IPAddress.Any, IPAddress.Broadcast);

            artNetSocket.NewPacket += ArtNetSocketOnNewPacket;
        }

        private void ArtNetSocketOnNewPacket(object sender, NewPacketEventArgs<ArtNetPacket> e)
        {
            if (e.Packet.OpCode == ArtNetOpCodes.Dmx)
            {
                var packet = e.Packet as ArtNetDmxPacket;

                if (packet.Universe != (_settings.UniverseNumber - 1))
                {
                    return;
                }

                foreach (var midiLink in _settings.MidiLinks)
                {
                    var DMXValue = (int)packet.DmxData[midiLink.DMXChannel - 1];
                    var MIDIChannel = midiLink.MIDIChannelOffset + midiLink.MIDIChannel;

                    var scaled = ConvertRange(0, 255, 0, 127, DMXValue);

                    DeskMidiDevice.Send(MidiMessage.ChangeControl(99, (MIDIChannel), 1).RawData);
                    DeskMidiDevice.Send(MidiMessage.ChangeControl(98, 23, 1).RawData);
                    DeskMidiDevice.Send(MidiMessage.ChangeControl(6, scaled, 1).RawData);
                    DeskMidiDevice.Send(MidiMessage.ChangeControl(38, 7, 1).RawData);

                    midiLink.UpdateValues(DMXValue, scaled);
                }
            }
        }

        

        private void BuildMidiDropdown()
        {
            int numDe = MidiOut.NumberOfDevices;

            for (int i = 0; numDe > i; i++)
            {
                var mop = MidiOut.DeviceInfo(i);
                var MIDIDeviceItem = new ComboBoxItem
                {
                    Content = mop.ProductName,
                    Tag = i
                };


                MidiDevicesComboBox.Items.Add(MIDIDeviceItem);
            }

            MidiDevicesComboBox.SelectedIndex = 0;
        }

        public int ConvertRange(int originalStart, int originalEnd, int newStart, int newEnd, int value)
        {
            double scale = (double)(newEnd - newStart) / (originalEnd - originalStart);
            return (int)(newStart + ((value - originalStart) * scale));
        }

        private void AddControlItemButton_OnClick(object sender, RoutedEventArgs e)
        {
            var midiLink = new MidiLink();
            AddControlLinkItem(midiLink);
            _settings.MidiLinks.Add(midiLink);
        }

        private void AddControlLinkItem(MidiLink midiLink)
        {
            var controlLink = new UserControls.ControlLink(midiLink);
            ControlItemStackPanel.Children.Add(controlLink);
            controlLink.OnRemoveClicked += link =>
            {
                _settings.MidiLinks.Remove(link._midiLink);
                ControlItemStackPanel.Children.Remove(link);
            };
        }

        private void SaveButton_OnClickButton_Click(object sender, RoutedEventArgs e)
        {
            var jsonData = JObject.FromObject(_settings);

            File.WriteAllText(FilePath, jsonData.ToString());
        }

    }
}
